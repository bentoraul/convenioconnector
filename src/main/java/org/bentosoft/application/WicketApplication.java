package org.bentosoft.application;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.settings.ExceptionSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.bentosoft.page.home.HomePage;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

public class WicketApplication extends WebApplication {

    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    @Override
    public void init() {
        super.init();
        configuration();
        errorPagesConfiguration();
        new AnnotatedMountScanner().scanPackage("org.bentosoft.page").mount(this);
        getComponentInstantiationListeners().add(new SpringComponentInjector(this));
    }

    private void configuration() {
        getMarkupSettings().setStripWicketTags(true);
        getPageSettings().setVersionPagesByDefault(false);
        getApplicationSettings().setUploadProgressUpdatesEnabled(true);
        getStoreSettings().setInmemoryCacheSize(0);
        getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
        getDebugSettings().setAjaxDebugModeEnabled(false);
    }

    private void errorPagesConfiguration() {
        getApplicationSettings().setPageExpiredErrorPage(HomePage.class);
        getApplicationSettings().setAccessDeniedPage(HomePage.class);
        getApplicationSettings().setInternalErrorPage(HomePage.class);
        getExceptionSettings().setUnexpectedExceptionDisplay(ExceptionSettings.SHOW_INTERNAL_ERROR_PAGE);
    }
}
