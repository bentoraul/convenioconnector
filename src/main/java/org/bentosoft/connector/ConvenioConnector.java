package org.bentosoft.connector;

import java.io.IOException;

import org.bentosoft.model.Convenio;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ConvenioConnector {

    private static String getAddress(String codigoConvenio, String codigoMunicipio) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://www.portaldatransparencia.gov.br/convenios/DetalhaConvenio.asp?CodConvenio=");
        stringBuilder.append(codigoConvenio);
        stringBuilder.append("&TipoConsulta=0&UF=&CodMunicipio=").append(codigoMunicipio);
        stringBuilder.append("&CodOrgao=&Pagina=&Periodo=");
        return stringBuilder.toString();
    }

    public static Convenio get(String codigoConvenio, String codigoMunicipio) {
        try {
            Document doc = Jsoup.connect(getAddress(codigoConvenio, codigoMunicipio)).get();
            String nextContent = null;
            Convenio convenio = new Convenio();
            convenio.setCodigoConvenio(codigoConvenio);
            convenio.setCodigoMunicipio(codigoMunicipio);
            for (Element input : doc.select("body div table tbody td")) {
                if (nextContent != null) {
                    if (nextContent.toLowerCase().contains("Situação".toLowerCase())) {
                        convenio.setSituacao(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Nº Original".toLowerCase())) {
                        convenio.setNumeroOriginal(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Concedente".toLowerCase())) {
                        convenio.setConcedente(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Valor Convênio".toLowerCase())) {
                        convenio.setValorConvenio(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Valor Contrapartida".toLowerCase())) {
                        convenio.setValorContrapartida(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Fim da Vigência".toLowerCase())) {
                        convenio.setFimVigencia(input.text().trim());
                    } else if (nextContent.toLowerCase().contains("Publicação".toLowerCase())) {
                        convenio.setPublicacao(input.text().trim());
                    }
                    nextContent = null;
                }
                if (input.attr("class").contains("tituloDetalhe")) {
                    nextContent = input.text();
                }
            }
            return convenio;
        } catch (IOException e) {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(get("787111", "1273"));
    }

}
