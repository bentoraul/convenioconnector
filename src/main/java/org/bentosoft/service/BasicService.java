package org.bentosoft.service;

import java.io.Serializable;
import java.util.List;

import org.bentosoft.dao.BasicDao;
import org.bentosoft.model.BaseEntity;

public interface BasicService<E extends BaseEntity> extends Serializable {

    void criar(E entidade);

    E ler(Serializable id);

    void atualizar(E entidade);

    void deletar(E entidade);

    BasicDao<E> getDao();

    List<E> listar();
}
