package org.bentosoft.service.impl;

import org.bentosoft.dao.BasicDao;
import org.bentosoft.dao.ConvenioDao;
import org.bentosoft.model.Convenio;
import org.bentosoft.service.ConvenioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("convenioService")
public class ConvenioServiceImpl extends BasicServiceImpl<Convenio> implements ConvenioService {

    private static final long serialVersionUID = 4644531345491873135L;

    @Autowired
    private ConvenioDao dao;

    @Override
    public BasicDao<Convenio> getDao() {
        return dao;
    }

}
