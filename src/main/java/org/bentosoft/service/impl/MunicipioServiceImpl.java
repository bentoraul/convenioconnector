package org.bentosoft.service.impl;

import org.bentosoft.dao.MunicipioDao;
import org.bentosoft.model.Municipio;
import org.bentosoft.service.MunicipioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("municipioService")
public class MunicipioServiceImpl extends BasicServiceImpl<Municipio> implements MunicipioService {

    private static final long serialVersionUID = 7293332679848254980L;

    @Autowired
    private MunicipioDao dao;

    @Override
    public MunicipioDao getDao() {
        System.out.println("Entrou no service");
        return dao;
    }

}
