package org.bentosoft.service.impl;

import java.io.Serializable;
import java.util.List;

import org.bentosoft.dao.BasicDao;
import org.bentosoft.model.BaseEntity;
import org.bentosoft.service.BasicService;

public abstract class BasicServiceImpl<E extends BaseEntity> implements BasicService<E> {

    private static final long serialVersionUID = 1940839592333210645L;

    @Override
    public abstract BasicDao<E> getDao();

    @Override
    public void criar(E entidade) {
        getDao().criar(entidade);
    }

    @Override
    public E ler(Serializable id) {
        return getDao().ler(id);
    }

    @Override
    public void atualizar(E entidade) {
        getDao().atualizar(entidade);
    }

    @Override
    public void deletar(E entidade) {
        getDao().deletar(entidade);
    }

    @Override
    public List<E> listar() {
        return getDao().listar();
    }

}
