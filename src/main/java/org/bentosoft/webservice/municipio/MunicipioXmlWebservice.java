package org.bentosoft.webservice.municipio;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.bentosoft.model.Municipio;
import org.bentosoft.service.MunicipioService;

/**
 * 
 * ContratoJsonWebservice: Configuração do webservice que retorna um Xml de convênio.
 * 
 * @author Raul Silveira Bento (bentoraul@gmail.com)
 *
 */
@Path("/municipio/xml")
public class MunicipioXmlWebservice {

    {
        SpringComponentInjector.get().inject(this);
    }

    @Inject
    private MunicipioService municipioService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_XML)
    public List<Municipio> getContrato() {
        List<Municipio> municipios = municipioService.listar();
        return municipios;
    }

}
