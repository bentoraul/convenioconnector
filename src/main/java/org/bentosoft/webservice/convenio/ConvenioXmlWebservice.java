package org.bentosoft.webservice.convenio;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.bentosoft.connector.ConvenioConnector;
import org.bentosoft.model.Convenio;
import org.bentosoft.model.primarykey.ConvenioPK;
import org.bentosoft.service.ConvenioService;

/**
 * 
 * ContratoJsonWebservice: Configuração do webservice que retorna um Xml de convênio.
 * 
 * @author Raul Silveira Bento (bentoraul@gmail.com)
 *
 */
@Path("/convenio/xml")
public class ConvenioXmlWebservice {
    
    {
        SpringComponentInjector.get().inject(this);
    }

    @Inject
    private ConvenioService convenioService;

    @GET
    @Path("/cidade={cidade}&convenio={convenio}")
    @Produces(MediaType.APPLICATION_XML)
    public Convenio getContrato(@PathParam("cidade") String cidade, @PathParam("convenio") String codigoConvenio) {
        Convenio convenio = convenioService.ler(new ConvenioPK(codigoConvenio, cidade));
        if (convenio == null) {
            convenio = ConvenioConnector.get(codigoConvenio, cidade);
            if (convenio != null) {
                convenioService.criar(convenio);
            }
        }
        return convenio;
    }

}
