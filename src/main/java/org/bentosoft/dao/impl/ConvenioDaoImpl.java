package org.bentosoft.dao.impl;

import org.bentosoft.dao.ConvenioDao;
import org.bentosoft.model.Convenio;
import org.springframework.stereotype.Repository;

@Repository("convenioDao")
public class ConvenioDaoImpl extends BasicDaoImpl<Convenio> implements ConvenioDao {

    private static final long serialVersionUID = -8650088641260876263L;

    public ConvenioDaoImpl() {
        super(Convenio.class);
    }

}
