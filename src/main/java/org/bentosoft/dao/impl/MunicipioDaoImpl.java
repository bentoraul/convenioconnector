package org.bentosoft.dao.impl;

import org.bentosoft.dao.MunicipioDao;
import org.bentosoft.model.Municipio;
import org.springframework.stereotype.Repository;

@Repository("municipioDao")
public class MunicipioDaoImpl extends BasicDaoImpl<Municipio> implements MunicipioDao {

    private static final long serialVersionUID = -8563849667398730586L;

    public MunicipioDaoImpl() {
        super(Municipio.class);
        System.out.println("Entrou no DAO");
    }

}
