package org.bentosoft.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.bentosoft.dao.BasicDao;
import org.bentosoft.model.BaseEntity;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class BasicDaoImpl<E extends BaseEntity> implements BasicDao<E> {

    private static final long serialVersionUID = -128490874767207985L;

    @PersistenceContext
    private EntityManager entityManager;

    private final Class<E> entityClass;

    protected BasicDaoImpl(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    protected void start() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void criar(E entidade) {
        entityManager.persist(entidade);
    }

    @Override
    public List<E> listar() {
        return entityManager.createQuery(String.format("FROM %s", entityClass.getName()), entityClass).getResultList();

    }

    @Override
    public E ler(Serializable id) {
        E entidade = entityManager.find(entityClass, id);
        return entidade;

    }

    @Override
    public void atualizar(E entidade) {
        entityManager.merge(entidade);
    }

    @Override
    public void deletar(E entidade) {
        entityManager.remove(entidade);
    }

}
