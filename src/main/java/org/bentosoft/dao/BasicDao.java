package org.bentosoft.dao;

import java.io.Serializable;
import java.util.List;

import org.bentosoft.model.BaseEntity;

public interface BasicDao<E extends BaseEntity> extends Serializable {

    void criar(E entidade);

    E ler(Serializable id);

    void atualizar(E entidade);

    void deletar(E entidade);

    List<E> listar();
}
