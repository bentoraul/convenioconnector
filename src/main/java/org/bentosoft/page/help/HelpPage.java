package org.bentosoft.page.help;

import org.apache.wicket.markup.html.WebPage;
import org.wicketstuff.annotation.mount.MountPath;

/**
 * HelpPage: Página de ajuda do sistema, onde pode-se ver os tipos de webservices.
 * 
 * @author Raul Silveira Bento (bentoraul@gmail.com)
 *
 */
@MountPath(value = "help")
public class HelpPage extends WebPage {

    private static final long serialVersionUID = -3765101102177327172L;

}
