package org.bentosoft.page.home;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.bentosoft.connector.ConvenioConnector;
import org.bentosoft.model.Convenio;
import org.bentosoft.model.Municipio;
import org.bentosoft.model.primarykey.ConvenioPK;
import org.bentosoft.service.ConvenioService;
import org.bentosoft.service.MunicipioService;

public class HomePage extends WebPage {

    @SpringBean
    private static MunicipioService municipioService;

    @SpringBean
    private static ConvenioService convenioService;

    private static final long serialVersionUID = 3496635488067970256L;

    private Convenio convenio;

    private Form<Void> formVoid;

    public HomePage(final PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        addOrReplace(formVoid = new Form<Void>("form") {

            private static final long serialVersionUID = 1552597293994014996L;

            private TextField<String> codigoConvenio;

            private DropDownChoice<Municipio> codigoMunicipio;

            private WebMarkupContainer detalhes;

            private WebMarkupContainer feedbackPanelContainer;

            private WebMarkupContainer getConvenioContainer(Convenio innerConvenio) {
                return detalhes = new WebMarkupContainer("detalhes") {

                    private static final long serialVersionUID = 270328824848871122L;

                    @Override
                    protected void onConfigure() {
                        super.onConfigure();
                        setVisible(innerConvenio.getCodigoConvenio() != null);
                    }

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setOutputMarkupPlaceholderTag(true);
                        addOrReplace(new Label("codigoConvenio", innerConvenio.getCodigoConvenio()));
                        addOrReplace(new Label("numeroOriginal", innerConvenio.getNumeroOriginal()));
                        addOrReplace(new Label("situacao", innerConvenio.getSituacao()));
                        addOrReplace(new Label("concedente", innerConvenio.getConcedente()));
                        addOrReplace(new Label("valorConvenio", innerConvenio.getValorConvenio()));
                        addOrReplace(new Label("publicacao", innerConvenio.getPublicacao()));
                        addOrReplace(new Label("valorContrapartida", innerConvenio.getValorContrapartida()));
                        addOrReplace(new Label("fimVigencia", innerConvenio.getFimVigencia()));
                    }

                };
            }

            @Override
            protected void onInitialize() {
                super.onInitialize();

                addOrReplace(feedbackPanelContainer = new WebMarkupContainer("feedbackPanelContainer") {

                    private static final long serialVersionUID = 3171545596989096314L;

                    private FeedbackPanel feedbackPanel;

                    @Override
                    protected void onConfigure() {
                        super.onConfigure();
                        setVisible(feedbackPanel.anyErrorMessage());
                    }

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setOutputMarkupPlaceholderTag(true);
                        addOrReplace(feedbackPanel = new FeedbackPanel("feedbackPanel"));
                    }

                });

                addOrReplace(codigoConvenio = new TextField<String>("convenio", Model.of("")) {

                    private static final long serialVersionUID = -7129853675024997941L;

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setOutputMarkupId(true);
                    }
                });
                addOrReplace(codigoMunicipio = new DropDownChoice<Municipio>("municipio") {

                    private static final long serialVersionUID = -3133643946966858915L;

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setOutputMarkupId(true);
                        setModel(new Model<Municipio>());
                        List<Municipio> municipios = municipioService.listar();
                        municipios.sort((a, b) -> a.getNome().compareTo(b.getNome()));
                        setChoices(municipios);
                        setChoiceRenderer(new ChoiceRenderer<>("nome", "id"));
                    }
                });
                addOrReplace(new AjaxButton("buscar") {

                    private static final long serialVersionUID = -7167304345665540613L;

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setDefaultFormProcessing(false);
                    }

                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        convenio = convenioService.ler(new ConvenioPK(codigoConvenio.getValue(), codigoMunicipio.getValue()));
                        if (convenio == null) {
                            convenio = ConvenioConnector.get(codigoConvenio.getValue(), codigoMunicipio.getValue());
                            if (convenio != null) {
                                convenioService.criar(convenio);
                            } else {
                                convenio = new Convenio();
                                error("Convênio Não Encontrado.");
                            }
                        }
                        formVoid.addOrReplace(getConvenioContainer(convenio));
                        target.add(detalhes, feedbackPanelContainer);
                    };
                });

                addOrReplace(new AjaxButton("atualizar") {

                    private static final long serialVersionUID = -7167304345665540613L;

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setDefaultFormProcessing(false);
                    }

                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        convenio = ConvenioConnector.get(codigoConvenio.getValue(), codigoMunicipio.getValue());
                        if (convenio != null) {
                            convenioService.atualizar(convenio);
                        } else {
                            convenio = new Convenio();
                            error("Convênio Não Encontrado.");
                        }
                        formVoid.addOrReplace(getConvenioContainer(convenio));
                        target.add(detalhes, feedbackPanelContainer);
                    };
                });

                addOrReplace(new AjaxButton("limpar") {

                    private static final long serialVersionUID = -7167304345665540613L;

                    @Override
                    protected void onInitialize() {
                        super.onInitialize();
                        setDefaultFormProcessing(false);
                    }

                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        convenio = new Convenio();
                        codigoConvenio.clearInput();
                        codigoMunicipio.clearInput();
                        formVoid.addOrReplace(getConvenioContainer(convenio));
                        target.add(detalhes, feedbackPanelContainer, codigoConvenio, codigoMunicipio);
                    };
                });
                addOrReplace(getConvenioContainer(new Convenio()));
            }
        });

    }

}
