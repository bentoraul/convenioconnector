package org.bentosoft.model.primarykey;

import java.io.Serializable;

public class ConvenioPK implements Serializable {

	private static final long serialVersionUID = -6506258575649310733L;

	private String codigoConvenio;

	private String codigoMunicipio;

	public ConvenioPK(String codigoConvenio, String codigoMunicipio) {
		super();
		this.codigoConvenio = codigoConvenio;
		this.codigoMunicipio = codigoMunicipio;
	}

	public ConvenioPK() {
	}

	@Override
	public int hashCode() {
		final Integer prime = 31;
		int result = 1;
		result = prime * result + ((codigoConvenio == null) ? 0 : codigoConvenio.hashCode());
		result = prime * result + ((codigoMunicipio == null) ? 0 : codigoMunicipio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConvenioPK other = (ConvenioPK) obj;
		if (codigoConvenio == null) {
			if (other.codigoConvenio != null)
				return false;
		} else if (!codigoConvenio.equals(other.codigoConvenio))
			return false;
		if (codigoMunicipio == null) {
			if (other.codigoMunicipio != null)
				return false;
		} else if (!codigoMunicipio.equals(other.codigoMunicipio))
			return false;
		return true;
	}

}
