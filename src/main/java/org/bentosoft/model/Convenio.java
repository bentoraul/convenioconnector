package org.bentosoft.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.xml.bind.annotation.XmlRootElement;

import org.bentosoft.model.primarykey.ConvenioPK;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Entity(name = "Convenio")
@IdClass(ConvenioPK.class)
@XmlRootElement(name = "convenio")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Convenio implements BaseEntity {

    private static final long serialVersionUID = 7060486029132590105L;

    @Id
    @Column(name = "CODIGO_CONVENIO", length = 25)
    private String codigoConvenio;

    @Id
    @Column(name = "CODIGO_MINICIPIO", length = 25)
    private String codigoMunicipio;

    @Column(name = "NUMERO_ORIGINAL", length = 100)
    private String numeroOriginal;

    @Column(name = "SITUACAO", length = 100)
    private String situacao;

    @Column(name = "CONCEDENTE", length = 100)
    private String concedente;

    @Column(name = "VALOR_DO_CONVENIO", length = 100)
    private String valorConvenio;

    @Column(name = "PUBLICAOCAO", length = 100)
    private String publicacao;

    @Column(name = "VALOR_DA_CONTRAPARTIDA", length = 100)
    private String valorContrapartida;

    @Column(name = "FIM_DA_VIGENCIA", length = 100)
    private String fimVigencia;

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getNumeroOriginal() {
        return numeroOriginal;
    }

    public void setNumeroOriginal(String numeroOriginal) {
        this.numeroOriginal = numeroOriginal;
    }

    public String getConcedente() {
        return concedente;
    }

    public void setConcedente(String concedente) {
        this.concedente = concedente;
    }

    public String getValorConvenio() {
        return valorConvenio;
    }

    public void setValorConvenio(String valorConvenio) {
        this.valorConvenio = valorConvenio;
    }

    public String getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(String publicacao) {
        this.publicacao = publicacao;
    }

    public String getValorContrapartida() {
        return valorContrapartida;
    }

    public void setValorContrapartida(String valorContrapartida) {
        this.valorContrapartida = valorContrapartida;
    }

    public String getFimVigencia() {
        return fimVigencia;
    }

    public void setFimVigencia(String fimVigencia) {
        this.fimVigencia = fimVigencia;
    }

    @Override
    public String toString() {
        return "Convenio [numeroOriginal=" + numeroOriginal + ", situacao=" + situacao + ", concedente=" + concedente + ", valorConvenio=" + valorConvenio + ", publicacao=" + publicacao + ", valorContrapartida=" + valorContrapartida + ", fimVigencia=" + fimVigencia + "]";
    }

}
