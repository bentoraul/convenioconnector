package org.bentosoft.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@Entity(name = "Municipio")
@XmlRootElement(name = "municipio")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Municipio implements BaseEntity {

    private static final long serialVersionUID = 1308412148479397095L;

    @Id
    @Column(name = "ID", length = 25)
    private String id;

    @Column(name = "NOME", length = 25)
    private String nome;

    @Column(name = "UF", length=2)
    private String uf;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

}
